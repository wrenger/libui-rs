use std::ffi;
use std::marker::PhantomData;
use std::os::raw::{c_int, c_void};

use super::{Window, UI};

use libui_sys as ui;

/// `MenuItem` which is part of a `Menu`.
#[derive(Debug)]
pub struct MenuItem<'a> {
    raw: *mut ui::uiMenuItem,
    _phantom: PhantomData<&'a mut ()>,
}

impl<'a> MenuItem<'a> {

    /// Creates a new MenuItem from the corresponding libui pointer.
    ///
    /// # Safety
    /// The pointer has to be a valid uiMenuItem pointer.
    pub unsafe fn from_raw(raw: *mut ui::uiMenuItem) -> MenuItem<'a> {
        MenuItem {
            raw,
            _phantom: PhantomData,
        }
    }

    /// Enables the `MenuItem` so that the user can interact with it.
    pub fn enable(&mut self) {
        unsafe { ui::uiMenuItemEnable(self.raw) };
    }

    /// Disables the `MenuItem` which prevents user interactions.
    pub fn disable(&mut self) {
        unsafe { ui::uiMenuItemDisable(self.raw) };
    }

    /// Returns whether the `MenuItem` is checked.
    pub fn checked(&self) {
        unsafe { ui::uiMenuItemChecked(self.raw) };
    }

    /// Sets whether the `MenuItem` is checked.
    pub fn set_checked(&mut self, val: bool) {
        unsafe { ui::uiMenuItemSetChecked(self.raw, val as c_int) };
    }

    /// Registers `f` to be run when the user clicks the `MenuItem`.
    ///
    /// Only one function can be registered at a time.
    pub fn on_clicked<F: FnMut(&mut MenuItem, &mut Window)>(&mut self, f: F) {
        extern "C" fn c_callback<F: FnMut(&mut MenuItem, &mut Window)>(
            item: *mut ui::uiMenuItem,
            window: *mut ui::uiWindow,
            data: *mut c_void,
        ) {
            let closure: &mut F = unsafe { &mut *(data as *mut F) };
            let mut window = unsafe { Window::from_raw(window) };
            let mut item = unsafe { MenuItem::from_raw(item) };
            (*closure)(&mut item, &mut window);
        }

        let data = Box::into_raw(Box::new(f)) as *mut c_void;
        unsafe { ui::uiMenuItemOnClicked(self.raw, Some(c_callback::<F>), data) }
    }
}

/// `Menu` represents a menu entry on the global menu bar of the application,
/// which is the same for each window. It may contain multiple `MenuItems`.
///
/// Menu bars can be displayed inside `Windows` or on MacOs at the top of the
/// screen.
#[derive(Debug)]
pub struct Menu {
    raw: *mut ui::uiMenu,
}

impl Menu {
    pub unsafe fn from_raw(raw: *mut ui::uiMenu) -> Menu {
        Menu { raw }
    }

    /// Creates a new `Menu` which is appended to the global menu bar.
    pub fn new(_ui: &UI, name: &str) -> Menu {
        let name = ffi::CString::new(name).unwrap();
        unsafe { Menu::from_raw(ui::uiNewMenu(name.as_c_str().as_ptr())) }
    }

    /// Add a new `MenuItem` with the given `name` to the Menu.
    pub fn append<'a>(&'a mut self, name: &str) -> MenuItem<'a> {
        let name = ffi::CString::new(name).unwrap();
        unsafe { MenuItem::from_raw(ui::uiMenuAppendItem(self.raw, name.as_c_str().as_ptr())) }
    }

    /// Adds a separator to the Menu.
    pub fn append_separator(&mut self) {
        unsafe { ui::uiMenuAppendSeparator(self.raw) };
    }

    /// Add a new check `MenuItem` with the given `name` and a checkbox to the Menu.
    pub fn append_check<'a>(&'a mut self, name: &str) -> MenuItem<'a> {
        let name = ffi::CString::new(name).unwrap();
        unsafe {
            MenuItem::from_raw(ui::uiMenuAppendCheckItem(
                self.raw,
                name.as_c_str().as_ptr(),
            ))
        }
    }

    /// Add a new quit `MenuItem` where its appearance is determined
    /// by the OS and its best practices.
    ///
    /// For this item to work the `UI::on_should_quit` callback has to be
    /// implemented, destroying all remaining windows.
    pub fn append_quit(&mut self) -> MenuItem {
        unsafe { MenuItem::from_raw(ui::uiMenuAppendQuitItem(self.raw)) }
    }

    /// Add a new preferences `MenuItem` where its appearance is determined
    /// by the OS and its best practices.
    pub fn append_preferences(&mut self) -> MenuItem {
        unsafe { MenuItem::from_raw(ui::uiMenuAppendPreferencesItem(self.raw)) }
    }

    /// Add a new about `MenuItem` where its appearance is determined
    /// by the OS and its best practices.
    pub fn append_about(&mut self) -> MenuItem {
        unsafe { MenuItem::from_raw(ui::uiMenuAppendAboutItem(self.raw)) }
    }
}
