//! # Basic UI Widgets
use std::ffi::CStr;
use std::os::raw::c_void;

use libui_sys as ui;

#[macro_use]
mod control_macro;

mod window;
pub use window::Window;
mod container;
pub use container::*;
mod display;
pub use display::*;
mod button;
pub use button::*;
mod input;
pub use input::*;
mod table;
pub use table::*;

use super::{UIError, UI, UI_TOKEN_REF};

/// Control represents a GUI control.
///
/// Control is an implementation of ControlImpl that provides all the methods
/// that Control requires.
#[derive(Debug)]
pub struct Control {
    raw: *mut ui::uiControl,
}

impl ControlImpl for Control {
    fn ptr(&self) -> *mut ui::uiControl {
        self.raw as *mut ui::uiControl
    }
}

impl Control {
    /// Creates a new Control from the corresponding libui pointer.
    ///
    /// # Safety
    /// The pointer has to be a valid pointer for this type of Control.
    pub unsafe fn from_raw(raw: *mut ui::uiControl) -> Control {
        Control { raw }
    }

    /// Allocates the memory for the given type of control.
    ///
    /// # Safety
    /// A non safe c memory allocation is being performed which may lead to
    /// unexpected behavior.
    /// The returned Control may be invalid.
    pub unsafe fn alloc(
        n: usize,
        type_sig: u32,
        typename: &CStr,
        functions: &'static ui::uiControlFunctions,
    ) -> Result<Control, UIError> {
        let raw = ui::uiAllocControl(
            n as u64,
            type_sig,
            typename.as_ptr(),
            functions as *const _ as *mut _,
        );
        if !raw.is_null() {
            Ok(Control::from_raw(raw))
        } else {
            Err(UIError::UnknownControl)
        }
    }

    /// Creates a handle that can be shared between threads
    pub fn thread_handle(&mut self) -> ControlHandle<Control> {
        ControlHandle {
            control: self as *mut _ as *mut Control,
        }
    }
}

/// ControlImpl provdes the methods common to all Controls.
pub trait ControlImpl {
    /// Return the pointer to the underlaying libui data structure.
    fn ptr(&self) -> *mut ui::uiControl;

    /// Destroys the ui control and frees the allocated memory.
    /// # Safety
    /// Beware use after free.
    fn destroy(&mut self) {
        unsafe { ui::uiControlDestroy(self.ptr()) };
    }

    /// Returns whether the `Control` is visible.
    fn visible(&self) -> bool {
        unsafe { ui::uiControlVisible(self.ptr()) != 0 }
    }

    /// Changes the visibility on this `Control` and it's children.
    /// Hidden controls do not participate in layout
    /// (that is, `LayoutBox`, `Grid`, etc. does not reserve space for hidden controls).
    fn set_visible(&mut self, visible: bool) {
        if visible {
            unsafe { ui::uiControlShow(self.ptr()) };
        } else {
            unsafe { ui::uiControlHide(self.ptr()) };
        }
    }

    /// Returns whether the `Control` is enabled
    fn enabled(&self) -> bool {
        unsafe { ui::uiControlEnabled(self.ptr()) != 0 }
    }

    /// Enables or disables the `Control`.
    /// If a control is disabled a user cannot interact with it.
    fn set_enabled(&mut self, enabled: bool) {
        if enabled {
            unsafe { ui::uiControlEnable(self.ptr()) };
        } else {
            unsafe { ui::uiControlDisable(self.ptr()) };
        }
    }

    /// Returns the parent control if any.
    fn parent(&self) -> Option<Control> {
        let ptr = unsafe { ui::uiControlParent(self.ptr()) };
        if !ptr.is_null() {
            Some(unsafe { Control::from_raw(ptr) })
        } else {
            None
        }
    }

    /// TODO: Check if vendor implementation is finished.
    fn set_parent<C: ControlImpl>(&mut self, parent: &mut C) {
        unsafe { ui::uiControlSetParent(self.ptr(), parent.ptr()) };
    }

    /// Returns if this is a toplevel control which cannot have a parent.
    fn is_toplevel(&self) -> bool {
        unsafe { ui::uiControlToplevel(self.ptr()) != 0 }
    }

    /// Handle returns the OS-level handle that backs the
    /// Control. On OSs that use reference counting for
    /// controls, Handle does not increment the reference
    /// count; you are sharing package ui's reference.
    fn handle(&self) -> usize {
        unsafe { ui::uiControlHandle(self.ptr()) }
    }
}

/// Thread safe handle for interacting with controls from other threads.
#[derive(Debug)]
pub struct ControlHandle<T: ControlImpl> {
    control: *mut T,
}

struct ControlHandleData<T, F> {
    control: *mut T,
    f: F,
}

impl<T: ControlImpl> ControlHandle<T> {
    /// Queues `f` to be run on the GUI thread when next possible.
    ///
    /// This call returns immediately and doesn't wait until `f` is called.
    pub fn queue_main<F: FnOnce(&UI, &mut T) + Send>(&self, f: F) {
        extern "C" fn c_callback<T, F: FnOnce(&UI, &mut T) + Send>(data: *mut c_void) {
            if let Some(token) = UI_TOKEN_REF.with(|t| t.borrow().upgrade()) {
                let data = unsafe { Box::from_raw(data as *mut ControlHandleData<T, F>) };
                (data.f)(&UI { token }, unsafe { &mut *data.control });
            }
        }
        let fn_ptr = c_callback::<T, F>;
        let data = Box::into_raw(Box::new(ControlHandleData {
            control: self.control,
            f,
        })) as *mut c_void;
        unsafe { ui::uiQueueMain(Some(fn_ptr), data) }
    }
}

unsafe impl<T: ControlImpl> Send for ControlHandle<T> {}
