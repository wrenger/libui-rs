//! Bindings for the minimalist, cross-platform, widget set `libui`.
//!
//! The `libui` project is currently mid-alpha, see its
//! [repository](https://github.com/andlabs/libui) for more information.
//!
//! # Example
//!
//! After adding `libui` to the list of dependencies of your `Cargo.toml` the
//! library can be used as follows:
//!
//! ```no_run
//! use libui::*;
//!
//! let ui = UI::init().unwrap();
//!
//! let mut window = Window::new(&ui, "Hello", 320, 240, false);
//!
//! let mut label = Label::new(&ui, "Hello, libui!");
//!
//! let mut label_btn = Button::new(&ui, "Change Label");
//! let mut counter = 0;
//! label_btn.on_clicked(|_| {
//!     counter += 1;
//!     label.set_text(&format!("Button clicked {} times", counter));
//! });
//! let mut file_btn = Button::new(&ui, "Open File");
//! file_btn.on_clicked(|_| {
//!     window.open_file();
//! });
//!
//! let mut hbox = BoxLayout::vertical(&ui);
//! hbox.append(&mut label, true);
//! hbox.append(&mut label_btn, false);
//! hbox.append(&mut file_btn, false);
//!
//! window.set_margined(true);
//! window.set_child(&mut hbox);
//! window.set_visible(true);
//!
//! // Start main loop
//! ui.main();
//! ```

use std::cell::RefCell;
use std::error::Error;
use std::ffi;
use std::fmt::{self, Display};
use std::marker::PhantomData;
use std::os::raw::{c_char, c_int, c_void};
use std::rc::{Rc, Weak};

use libui_sys as ui;

mod controls;
pub use controls::*;
mod menu;
pub use menu::*;
pub mod draw;

#[inline]
fn ui_string(ptr: *mut c_char) -> String {
    let title = unsafe { ffi::CStr::from_ptr(ptr).to_string_lossy().into_owned() };
    unsafe { ui::uiFreeText(ptr) };
    title
}

/// Represents errors that may occur during initialization.
#[derive(Debug)]
pub enum UIError {
    FailedInit(String),
    UnknownControl,
}

impl Display for UIError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            UIError::FailedInit(msg) => write!(f, "UI Init failed: {}", msg),
            UIError::UnknownControl => write!(f, "UI Unknown Control"),
        }
    }
}

impl Error for UIError {}

#[derive(Debug)]
struct UIToken {
    _phantom: PhantomData<*mut ()>,
}

impl Drop for UIToken {
    fn drop(&mut self) {
        unsafe { ui::uiUninit() };
    }
}

thread_local! {
    static UI_TOKEN_REF: RefCell<Weak<UIToken>> = RefCell::new(Weak::default());
}

/// A handle to user interface functionality.
/// Before any other UI functionality can be used this interface to be initialized.
#[derive(Debug, Clone)]
pub struct UI {
    #[allow(dead_code)]
    token: Rc<UIToken>,
}

impl UI {
    /// Initializes the underlaying libui bindings.
    ///
    /// This handle cannot be shared between threads and only one instance can
    /// be active at once.
    ///
    /// Note: Applications based on Cocoa (the native MacOS GUI) require that the
    /// main thread controls the UI!
    pub fn init() -> Result<UI, UIError> {
        let mut o = ui::uiInitOptions {
            Size: std::mem::size_of::<ui::uiInitOptions>() as ui::size_t,
        };

        if UI_TOKEN_REF.with(|t| t.borrow().strong_count()) > 0 {
            return Err(UIError::FailedInit("The UI was already initialized".into()));
        }

        let err = unsafe { ui::uiInit(&mut o as *mut ui::uiInitOptions) };
        if !err.is_null() {
            let msg = unsafe { ffi::CStr::from_ptr(err) };
            let msg = msg.to_string_lossy().into_owned();
            unsafe { ui::uiFreeInitError(err) };
            return Err(UIError::FailedInit(msg));
        }
        let token = Rc::new(UIToken {
            _phantom: PhantomData,
        });

        UI_TOKEN_REF.with(|t| {
            *t.borrow_mut() = Rc::downgrade(&token);
        });

        Ok(UI { token })
    }

    /// Registers `f` to be run when the application quits.
    /// Returning `false` prevents the application from quitting.
    ///
    /// Only one function can be registered at a time.
    pub fn on_should_quit<F: FnMut() -> bool>(&self, f: F) {
        extern "C" fn c_callback<F: FnMut() -> bool>(data: *mut c_void) -> c_int {
            let closure = unsafe { &mut *(data as *mut F) };
            closure() as c_int
        }
        let data = Box::into_raw(Box::new(f)) as *mut c_void;
        unsafe { ui::uiOnShouldQuit(Some(c_callback::<F>), data) }
    }

    /// Registers `f` to be run every `millis` milliseconds on the GUI thread.
    ///
    /// Only one function can be registered at a time.
    pub fn on_delay<F: FnMut() -> bool>(&self, millis: usize, f: F) {
        extern "C" fn c_callback<F: FnMut() -> bool>(data: *mut c_void) -> c_int {
            let closure = unsafe { &mut *(data as *mut F) };
            closure() as c_int
        }
        let data = Box::into_raw(Box::new(f)) as *mut c_void;
        unsafe { ui::uiTimer(millis as c_int, Some(c_callback::<F>), data) }
    }

    /// Executes the GUI main loop.
    pub fn main(&self) {
        unsafe { ui::uiMain() };
    }

    /// Quit queues a return from the main loop.
    /// It should not be called before the main loop has been started (`UI::main`).
    pub fn quit(&self) {
        unsafe { ui::uiQuit() };
    }
}
