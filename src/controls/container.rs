use std::ffi;
use std::os::raw::{c_int, c_uint};

use super::{Control, ControlHandle, ControlImpl};
use crate::{ui_string, UI};

use libui_sys as ui;

control! {
    /// `BoxLayout` is a `Control` that holds a group of Controls horizontally or vertically.
    /// If horizontally, then all controls have the same height.
    /// If vertically, then all controls have the same width.
    ///
    /// By default, each control has its preferred width (horizontal) or height (vertical);
    /// if a control is marked `stretchy`, it will take whatever space is left over.
    /// If multiple controls are marked stretchy, they will be given equal shares
    /// of the leftover space.
    ///
    /// There can also be space between each control (`padding`).
    BoxLayout,
    uiBox
}

impl BoxLayout {
    /// Creates a new horizontal `BoxLayout`.
    pub fn horizontal(_ui: &UI) -> BoxLayout {
        unsafe { BoxLayout::from_raw(ui::uiNewHorizontalBox()) }
    }

    /// Creates a new vertical `BoxLayout`.
    pub fn vertical(_ui: &UI) -> BoxLayout {
        unsafe { BoxLayout::from_raw(ui::uiNewVerticalBox()) }
    }

    /// Adds the given `Control` to the end of the `BoxLayout`.
    ///
    /// By default, each control has its preferred width (horizontal) or height (vertical);
    /// if a control is marked `stretchy`, it will take whatever space is left over.
    pub fn append<C: ControlImpl>(&mut self, child: &mut C, stretchy: bool) {
        unsafe { ui::uiBoxAppend(self.raw, child.ptr(), stretchy as c_int) };
    }

    /// Deletes the `i`th control of the `BoxLayout`.
    pub fn delete(&mut self, i: usize) {
        unsafe { ui::uiBoxDelete(self.raw, i as c_int) };
    }

    /// Returns whether there is space between each control of the `BoxLayout`.
    pub fn padded(&self) -> bool {
        unsafe { ui::uiBoxPadded(self.raw) != 0 }
    }

    /// Controls whether there is space between each control of the `BoxLayout`.
    ///
    /// The size of the padding is determined by the OS and its best practices.
    pub fn set_padded(&mut self, padded: bool) {
        unsafe { ui::uiBoxSetPadded(self.raw, padded as c_int) };
    }
}

control! {
    /// `Tab` is a `Control` that holds tabbed pages of `Control`s.
    /// Each tab has a label.
    /// The user can click on the tabs themselves to switch pages.
    /// Individual pages can also have margins.
    Tab,
    uiTab
}

impl Tab {
    /// Creates a new `Tab`.
    pub fn new(_ui: &UI) -> Tab {
        unsafe { Tab::from_raw(ui::uiNewTab()) }
    }

    /// Adds the given page to the end of the `Tab`.
    pub fn append<C: ControlImpl>(&mut self, name: &str, child: &mut C) {
        let name = ffi::CString::new(name).unwrap();
        unsafe { ui::uiTabAppend(self.raw, name.as_c_str().as_ptr(), child.ptr()) };
    }

    /// Adds the given page to the `Tab` such that it is the `i`th page of
    /// the `Tab` (starting at 0).
    pub fn insert<C: ControlImpl>(&mut self, name: &str, i: usize, child: &mut C) {
        let name = ffi::CString::new(name).unwrap();
        unsafe { ui::uiTabInsertAt(self.raw, name.as_c_str().as_ptr(), i as c_int, child.ptr()) };
    }

    /// Deletes the nth page of the `Tab`.
    pub fn delete(&mut self, i: usize) {
        unsafe { ui::uiTabDelete(self.raw, i as c_int) };
    }

    /// Returns whether page `i` (starting at 0) of the Tab has margins around its child.
    pub fn margined(&self, i: usize) -> bool {
        unsafe { ui::uiTabMargined(self.raw, i as c_int) != 0 }
    }

    /// Controls whether page n (starting at 0) of the Tab has margins around its child.
    ///
    /// The size of the margins are determined by the OS and its best practices.
    pub fn set_margined(&mut self, page: usize, val: bool) {
        unsafe { ui::uiTabSetMargined(self.raw, page as c_int, val as c_int) };
    }

    /// Returns the number of pages in the `Tab`.
    pub fn num_paged(&self) -> usize {
        unsafe { ui::uiTabNumPages(self.raw) as usize }
    }
}

control! {
    /// `Group` is a `Control` that holds another `Control` and wraps it around a
    /// labelled box (though some systems make this box invisible).
    /// You can use this to group related controls together.
    Group,
    uiGroup
}

impl Group {
    /// Creates a new `Group`.
    pub fn new(_ui: &UI, title: &str) -> Group {
        let title = ffi::CString::new(title).unwrap();
        unsafe { Group::from_raw(ui::uiNewGroup(title.as_c_str().as_ptr())) }
    }

    /// Sets the `Group`'s child to `child`.
    pub fn set_child<C: ControlImpl>(&mut self, child: &mut C) {
        unsafe { ui::uiGroupSetChild(self.raw, child.ptr()) };
    }

    /// Returns the `Group`'s title.
    pub fn title(&self) -> String {
        ui_string(unsafe { ui::uiGroupTitle(self.raw) })
    }

    /// Sets the Group's title to title.
    pub fn set_title(&mut self, title: &str) {
        let title = ffi::CString::new(title).unwrap();
        unsafe { ui::uiGroupSetTitle(self.raw, title.as_c_str().as_ptr()) }
    }

    /// Returns whether the `Group` has margins around its child.
    pub fn margined(&self) -> bool {
        unsafe { ui::uiGroupMargined(self.raw) != 0 }
    }

    /// Controls whether the `Group` has margins around its child.
    ///
    /// The size of the margins are determined by the OS and its best practices.
    pub fn set_margined(&mut self, val: bool) {
        unsafe { ui::uiGroupSetMargined(self.raw, val as c_int) };
    }
}

/// `GridAlign` represents the alignment of a `Control` in its cell of a `Grid`.
#[derive(Debug)]
pub enum GridAlign {
    Fill = 0,
    Start = 1,
    Center = 2,
    End = 3,
}

/// `GridAt` represents a side of a `Control` to add other `Controls` to a `Grid` to.
#[derive(Debug)]
pub enum GridAt {
    Leading = 0,
    Top = 1,
    Trailing = 2,
    Bottom = 3,
}

control! {
    /// `Grid` is a `Control` that arranges other Controls in a grid.
    /// `Grid` is a very powerful container:
    /// it can position and size each `Control` in several ways and can (and must)
    /// have Controls added to it in any direction.
    /// It can also have Controls spanning multiple rows and columns.
    ///
    /// Each `Control` in a `Grid` has associated `expansion` and `alignment`
    /// values in both the X and Y direction.
    /// Expansion determines whether all cells in the same row/column are given
    /// whatever space is left over after figuring out how big the rest of the `Grid` should be.
    /// Alignment determines the position of a `Control` relative to its cell after computing the above.
    /// The special alignment Fill can be used to grow a `Control` to fit its cell.
    /// Note that expansion and alignment are independent variables.
    ///
    /// For more information on expansion and alignment,
    /// read https://developer.gnome.org/gtk3/unstable/ch28s02.html.
    Grid,
    uiGrid
}

impl Grid {
    /// Creates a new `Grid`.
    pub fn new(_ui: &UI) -> Grid {
        unsafe { Grid::from_raw(ui::uiNewGrid()) }
    }

    /// Adds the given control to the `Grid`, at the given coordinate.
    #[allow(clippy::too_many_arguments)]
    pub fn append<C: ControlImpl>(
        &self,
        child: &mut C,
        left: usize,
        top: usize,
        x_span: usize,
        y_span: usize,
        h_expand: bool,
        h_align: GridAlign,
        v_expand: bool,
        valign: GridAlign,
    ) {
        unsafe {
            ui::uiGridAppend(
                self.raw,
                child.ptr(),
                left as c_int,
                top as c_int,
                x_span as c_int,
                y_span as c_int,
                h_expand as c_int,
                h_align as c_uint,
                v_expand as c_int,
                valign as c_uint,
            )
        };
    }

    /// Adds the given control to the `Grid` relative to an existing control.
    #[allow(clippy::too_many_arguments)]
    pub fn insert<C: ControlImpl, D: ControlImpl>(
        &self,
        child: &mut C,
        existing: &mut D,
        at: GridAt,
        x_span: usize,
        y_span: usize,
        h_expand: bool,
        h_align: GridAlign,
        v_expand: bool,
        valign: GridAlign,
    ) {
        unsafe {
            ui::uiGridInsertAt(
                self.raw,
                child.ptr(),
                existing.ptr(),
                at as c_uint,
                x_span as c_int,
                y_span as c_int,
                h_expand as c_int,
                h_align as c_uint,
                v_expand as c_int,
                valign as c_uint,
            )
        };
    }

    /// Returns whether there is space between each control of the `Grid`.
    pub fn padded(&self) -> bool {
        unsafe { ui::uiGridPadded(self.raw) != 0 }
    }

    /// Controls whether there is space between each control of the `Grid`.
    ///
    /// The size of the padding is determined by the OS and its best practices.
    pub fn set_padded(&self, val: bool) {
        unsafe { ui::uiGridSetPadded(self.raw, val as c_int) };
    }
}
