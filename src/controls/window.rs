use std::ffi;
use std::os::raw::{c_int, c_void};
use std::path::PathBuf;

use super::{Control, ControlHandle, ControlImpl};
use crate::{ui_string, UI};

use libui_sys as ui;

control! {
    /// `Window` is a Control that represents a top-level window.
    /// A `Window` contains one child `Control` that occupies the entirety of the window.
    /// Though a `Window` is a `Control`, a Window cannot be the child of another `Control`.
    Window,
    uiWindow
}

impl Window {
    /// Creates a new `Window` which may have a menubar.
    /// On default the Application quits when the window is closed.
    /// However, this can be changed by setting a custom `on_close` callback.
    pub fn new(ui: &UI, title: &str, width: usize, height: usize, menubar: bool) -> Window {
        let title = ffi::CString::new(title).unwrap();
        let mut window = unsafe {
            Window::from_raw(ui::uiNewWindow(
                title.as_c_str().as_ptr(),
                width as c_int,
                height as c_int,
                menubar as c_int,
            ))
        };
        window.on_closing(|_| {
            println!("Closing main window and exiting application...");
            ui.quit();
            true
        });
        window
    }

    /// Returns the `Window`'s title.
    pub fn title(&self) -> String {
        ui_string(unsafe { ui::uiWindowTitle(self.raw) })
    }

    /// Sets the `Window`'s title to `title`.
    pub fn set_title(&mut self, title: &str) {
        let title = ffi::CString::new(title).unwrap();
        unsafe { ui::uiWindowSetTitle(self.raw, title.as_c_str().as_ptr()) }
    }

    /// Returns the current size of `Window`'s content.
    pub fn size(&self) -> (usize, usize) {
        let (mut width, mut height) = (0, 0);
        unsafe { ui::uiWindowContentSize(self.raw, &mut width, &mut height) };
        (width as usize, height as usize)
    }

    /// Sets the size of `Window`'s content.
    pub fn set_size(&mut self, width: usize, height: usize) {
        unsafe { ui::uiWindowSetContentSize(self.raw, width as c_int, height as c_int) };
    }

    /// Returns whether the `Window`' is fullscreen.
    pub fn fullscreen(&self) -> bool {
        unsafe { ui::uiWindowFullscreen(self.raw) != 0 }
    }

    /// Sets the `Window`' to be fullscreen or not.
    pub fn set_fullscreen(&mut self, val: bool) {
        unsafe { ui::uiWindowSetFullscreen(self.raw, val as c_int) };
    }

    /// Returns whether the `Window` is borderless.
    pub fn borderless(&self) -> bool {
        unsafe { ui::uiWindowBorderless(self.raw) != 0 }
    }

    /// Sets the `Window` to be borderless or not.
    pub fn set_borderless(&mut self, val: bool) {
        unsafe { ui::uiWindowSetBorderless(self.raw, val as c_int) };
    }

    /// Returns whether the `Window` has margins around its child.
    pub fn margined(&self) -> bool {
        unsafe { ui::uiWindowMargined(self.raw) != 0 }
    }

    /// Controls whether the `Window` has margins around its child.
    /// The size of the margins are determined by the OS and its best practices.
    pub fn set_margined(&mut self, val: bool) {
        unsafe { ui::uiWindowSetMargined(self.raw, val as c_int) };
    }

    /// Sets the `Window`'s child to child.
    pub fn set_child<C: ControlImpl>(&mut self, child: &mut C) {
        unsafe { ui::uiWindowSetChild(self.raw, child.ptr()) };
    }

    /// TODO: Check if vendor implementation is finished.
    pub fn on_resize<F: FnMut(&mut Window)>(&mut self, f: F) {
        extern "C" fn c_callback<F: FnMut(&mut Window)>(
            window: *mut ui::uiWindow,
            data: *mut c_void,
        ) {
            let closure: &mut F = unsafe { &mut *(data as *mut F) };
            let mut window = unsafe { Window::from_raw(window) };
            (*closure)(&mut window);
        }

        let data = Box::into_raw(Box::new(f)) as *mut c_void;
        unsafe { ui::uiWindowOnContentSizeChanged(self.raw, Some(c_callback::<F>), data) }
    }

    /// Registers `f` to be run when the user clicks the `Window`'s close button.
    ///
    /// Only one function can be registered at a time.
    ///
    /// If `f` returns true, the window is destroyed with the `destroy` method.
    /// If `f` returns false, or if `on_closing` is never called,
    /// the window is not destroyed and is kept visible.
    pub fn on_closing<F: FnMut(&mut Window) -> bool>(&mut self, f: F) {
        extern "C" fn c_callback<F: FnMut(&mut Window) -> bool>(
            window: *mut ui::uiWindow,
            data: *mut c_void,
        ) -> c_int {
            let closure: &mut F = unsafe { &mut *(data as *mut F) };
            let mut window = unsafe { Window::from_raw(window) };
            let result = (*closure)(&mut window);
            result as c_int
        }

        let data = Box::into_raw(Box::new(f)) as *mut c_void;
        unsafe { ui::uiWindowOnClosing(self.raw, Some(c_callback::<F>), data) }
    }

    /// Creates an os specific file dialog for opening files.
    /// Returns the selected file or `None`.
    pub fn open_file(&self) -> Option<PathBuf> {
        let ptr = unsafe { ui::uiOpenFile(self.raw) };
        if !ptr.is_null() {
            Some(PathBuf::from(ui_string(ptr)))
        } else {
            None
        }
    }

    /// Opens an os specific file dialog for saving files.
    /// Returns the selected file or `None`.
    pub fn save_file(&self) -> Option<PathBuf> {
        let ptr = unsafe { ui::uiSaveFile(self.raw) };
        if !ptr.is_null() {
            Some(PathBuf::from(ui_string(ptr)))
        } else {
            None
        }
    }

    /// Displays the given `title` and `description` in a modal popup dialog.
    pub fn alert(&self, title: &str, description: &str) {
        let title = ffi::CString::new(title).unwrap();
        let description = ffi::CString::new(description).unwrap();
        unsafe {
            ui::uiMsgBox(
                self.raw,
                title.as_c_str().as_ptr(),
                description.as_c_str().as_ptr(),
            )
        };
    }

    /// Displays the given `title` and `description` in a modal error dialog.
    pub fn error(&self, title: &str, description: &str) {
        let title = ffi::CString::new(title).unwrap();
        let description = ffi::CString::new(description).unwrap();
        unsafe {
            ui::uiMsgBoxError(
                self.raw,
                title.as_c_str().as_ptr(),
                description.as_c_str().as_ptr(),
            )
        };
    }
}
