use std::ffi;
use std::os::raw::{c_int, c_void};

use super::{Control, ControlHandle, ControlImpl};
use crate::{ui_string, UI};

use libui_sys as ui;

control! {
    /// `Entry` is a `Control` that represents a space that the user can type
    /// a single line of text into.
    Entry,
    uiEntry
}

impl Entry {
    /// Creates a new `Entry`.
    pub fn new(_ui: &UI) -> Entry {
        unsafe { Entry::from_raw(ui::uiNewEntry()) }
    }

    /// Creates a new `Entry` whose contents are visibly obfuscated, suitable for passwords.
    pub fn password(_ui: &UI) -> Entry {
        unsafe { Entry::from_raw(ui::uiNewPasswordEntry()) }
    }

    /// Creates a new `Entry` suitable for searching with.
    /// Changed events may, depending on the system, be delayed with a search Entry,
    /// to produce a smoother user experience.
    pub fn search(_ui: &UI) -> Entry {
        unsafe { Entry::from_raw(ui::uiNewSearchEntry()) }
    }

    /// Returns the `Entry`'s text.
    pub fn text(&self) -> String {
        ui_string(unsafe { ui::uiEntryText(self.raw) })
    }

    /// Sets the `Entry`'s text to text.
    pub fn set_text(&mut self, text: &str) {
        let text = ffi::CString::new(text).unwrap();
        unsafe { ui::uiEntrySetText(self.raw, text.as_c_str().as_ptr()) };
    }

    /// Returns whether the Entry can be changed.
    pub fn read_only(&self) -> bool {
        unsafe { ui::uiEntryReadOnly(self.raw) != 0 }
    }

    /// Sets whether the `Entry` can be changed.
    pub fn set_read_only(&mut self, val: bool) {
        unsafe { ui::uiEntrySetReadOnly(self.raw, val as c_int) };
    }

    /// Registers `f` to be run when the user makes a change to the `Entry`.
    ///
    /// Only one function can be registered at a time.
    pub fn on_changed<F: FnMut(&mut Entry)>(&mut self, f: F) {
        extern "C" fn c_callback<F: FnMut(&mut Entry)>(
            control: *mut ui::uiEntry,
            data: *mut c_void,
        ) {
            let closure: &mut F = unsafe { &mut *(data as *mut F) };
            let mut control = unsafe { Entry::from_raw(control) };
            (*closure)(&mut control);
        }

        let data = Box::into_raw(Box::new(f)) as *mut c_void;
        unsafe { ui::uiEntryOnChanged(self.raw, Some(c_callback::<F>), data) }
    }
}

control! {
    /// Spinbox is a Control that represents a space where the user can enter integers.
    /// The space also comes with buttons to add or subtract 1 from the integer.
    ///
    /// Setting value outside of range will automatically clamp.
    /// Initial value is minimum.
    /// complaint if min >= max?
    Spinbox,
    uiSpinbox
}

impl Spinbox {
    /// Creates a new `Spinbox`. If min >= max, they are swapped.
    pub fn new(_ui: &UI, min: i32, max: i32) -> Spinbox {
        unsafe { Spinbox::from_raw(ui::uiNewSpinbox(min as c_int, max as c_int)) }
    }

    /// Returns the `Spinbox`'s current value.
    pub fn value(&self) -> i32 {
        unsafe { ui::uiSpinboxValue(self.raw) }
    }

    /// Sets the `Spinbox`'s current value to `value`.
    pub fn set_value(&mut self, value: i32) {
        unsafe { ui::uiSpinboxSetValue(self.raw, value) };
    }

    /// Registers `f` to be run when the user changes the value of the `Spinbox`.
    ///
    /// Only one function can be registered at a time.
    pub fn on_changed<F: FnMut(&mut Spinbox)>(&mut self, f: F) {
        extern "C" fn c_callback<F: FnMut(&mut Spinbox)>(
            control: *mut ui::uiSpinbox,
            data: *mut c_void,
        ) {
            let closure: &mut F = unsafe { &mut *(data as *mut F) };
            let mut control = unsafe { Spinbox::from_raw(control) };
            (*closure)(&mut control);
        }

        let data = Box::into_raw(Box::new(f)) as *mut c_void;
        unsafe { ui::uiSpinboxOnChanged(self.raw, Some(c_callback::<F>), data) }
    }
}

control! {
    /// `Slider` is a `Control` that represents a horizontal bar that represents a range of integers.
    /// The user can drag a pointer on the bar to select an integer.
    ///
    /// Setting value outside of range will automatically clamp.
    /// Initial value is minimum.
    /// complaint if min >= max?
    Slider,
    uiSlider
}

impl Slider {
    /// Creates a new `Slider`. If min >= max, they are swapped.
    pub fn new(_ui: &UI, min: i32, max: i32) -> Slider {
        unsafe { Slider::from_raw(ui::uiNewSlider(min as c_int, max as c_int)) }
    }

    /// Returns the `Slider`'s current value.
    pub fn value(&self) -> i32 {
        unsafe { ui::uiSliderValue(self.raw) }
    }

    /// Sets the `Slider`'s current value to `value`.
    pub fn set_value(&mut self, value: i32) {
        unsafe { ui::uiSliderSetValue(self.raw, value) };
    }

    /// Registers `f` to be run when the user changes the value of the `Slider`.
    ///
    /// Only one function can be registered at a time.
    pub fn on_changed<F: FnMut(&mut Slider)>(&mut self, f: F) {
        extern "C" fn c_callback<F: FnMut(&mut Slider)>(
            control: *mut ui::uiSlider,
            data: *mut c_void,
        ) {
            let closure: &mut F = unsafe { &mut *(data as *mut F) };
            let mut control = unsafe { Slider::from_raw(control) };
            (*closure)(&mut control);
        }

        let data = Box::into_raw(Box::new(f)) as *mut c_void;
        unsafe { ui::uiSliderOnChanged(self.raw, Some(c_callback::<F>), data) }
    }
}

control! {
    /// `Combobox` is a `Control` that represents a drop-down list of strings that the
    /// user can choose one of at any time.
    /// For a `Combobox` that users can type values into, see `EditableCombobox`.
    Combobox,
    uiCombobox
}

impl Combobox {
    /// Creates a new `Combobox`.
    pub fn new(_ui: &UI) -> Combobox {
        unsafe { Combobox::from_raw(ui::uiNewCombobox()) }
    }

    /// Adds the named `item` to the end of the `Combobox`.
    pub fn append(&mut self, item: &str) {
        let item = ffi::CString::new(item).unwrap();
        unsafe { ui::uiComboboxAppend(self.raw, item.as_c_str().as_ptr()) }
    }

    /// Returns the index of the currently selected item in the `Combobox`,
    /// or -1 if nothing is selected.
    pub fn selected(&self) -> isize {
        unsafe { ui::uiComboboxSelected(self.raw) as isize }
    }

    /// Sets the currently selected item in the `Combobox` to `index`.
    /// If `index` is -1 no item will be selected.
    pub fn set_selected(&mut self, index: isize) {
        unsafe { ui::uiComboboxSetSelected(self.raw, index as c_int) };
    }

    /// Registers `f` to be run when the user selects an item in the `Combobox`.
    ///
    /// Only one function can be registered at a time.
    pub fn on_changed<F: FnMut(&mut Combobox)>(&mut self, f: F) {
        extern "C" fn c_callback<F: FnMut(&mut Combobox)>(
            control: *mut ui::uiCombobox,
            data: *mut c_void,
        ) {
            let closure: &mut F = unsafe { &mut *(data as *mut F) };
            let mut control = unsafe { Combobox::from_raw(control) };
            (*closure)(&mut control);
        }

        let data = Box::into_raw(Box::new(f)) as *mut c_void;
        unsafe { ui::uiComboboxOnSelected(self.raw, Some(c_callback::<F>), data) }
    }
}

control! {
    /// `EditableCombobox` is a `Control` that represents a drop-down list of
    /// strings that the user can choose one of at any time.
    /// It also has an entry field that the user can type an alternate choice into.
    EditableCombobox,
    uiEditableCombobox
}

impl EditableCombobox {
    /// Creates a new `EditableCombobox`.
    pub fn new(_ui: &UI) -> EditableCombobox {
        unsafe { EditableCombobox::from_raw(ui::uiNewEditableCombobox()) }
    }

    /// Adds the named `item` to the end of the `EditableCombobox`.
    pub fn append(&mut self, item: &str) {
        let item = ffi::CString::new(item).unwrap();
        unsafe { ui::uiEditableComboboxAppend(self.raw, item.as_c_str().as_ptr()) }
    }

    /// Returns the text in the entry of the EditableCombobox,
    /// which could be one of the choices in the list if the user has selected one.
    pub fn text(&self) -> String {
        ui_string(unsafe { ui::uiEditableComboboxText(self.raw) })
    }

    /// Sets the text in the entry of the `EditableCombobox`.
    pub fn set_text(&mut self, text: &str) {
        let text = ffi::CString::new(text).unwrap();
        unsafe { ui::uiEditableComboboxSetText(self.raw, text.as_c_str().as_ptr()) };
    }

    /// Registers `f` to be run when the user either selects an item \
    /// or changes the text in the `EditableCombobox`.
    ///
    /// Only one function can be registered at a time.
    pub fn on_changed<F: FnMut(&EditableCombobox)>(&self, f: F) {
        extern "C" fn c_callback<F: FnMut(&EditableCombobox)>(
            control: *mut ui::uiEditableCombobox,
            data: *mut c_void,
        ) {
            let closure: &mut F = unsafe { &mut *(data as *mut F) };
            let mut control = unsafe { EditableCombobox::from_raw(control) };
            (*closure)(&mut control);
        }

        let data = Box::into_raw(Box::new(f)) as *mut c_void;
        unsafe { ui::uiEditableComboboxOnChanged(self.raw, Some(c_callback::<F>), data) }
    }
}

/// tm struct from <ctime.h>
#[repr(C)]
#[derive(Debug)]
pub struct DateTime {
    sec: c_int,
    min: c_int,
    hour: c_int,
    mday: c_int,
    mon: c_int,
    year: c_int,
    wday: c_int,
    yday: c_int,
    isdst: c_int,
}

impl Default for DateTime {
    fn default() -> DateTime {
        DateTime {
            sec: 0,
            min: 0,
            hour: 0,
            mday: 0,
            mon: 0,
            year: 0,
            wday: 0,
            yday: 0,
            isdst: 0,
        }
    }
}

control! {
    /// `DateTimePicker` is a `Control` that represents a field where the user can enter a date and/or a time.
    ///
    /// * TODO document that tm_wday and tm_yday are undefined, and tm_isdst should be -1
    /// * TODO document that for both sides
    /// * TODO document time zone conversions or lack thereof
    DateTimePicker,
    uiDateTimePicker
}

impl DateTimePicker {
    /// Creates a new `DateTimePicker` that shows both a date and a time.
    pub fn datetime(_ui: &UI) -> DateTimePicker {
        unsafe { DateTimePicker::from_raw(ui::uiNewDateTimePicker()) }
    }

    /// Creates a new `DateTimePicker` that shows only a date.
    pub fn date(_ui: &UI) -> DateTimePicker {
        unsafe { DateTimePicker::from_raw(ui::uiNewTimePicker()) }
    }

    /// Creates a new `DateTimePicker` that shows only a time.
    pub fn time(_ui: &UI) -> DateTimePicker {
        unsafe { DateTimePicker::from_raw(ui::uiNewTimePicker()) }
    }

    /// Returns the time stored in the `DateTimePicker`.
    /// The time is assumed to be local time.
    pub fn value(&self) -> DateTime {
        let mut time = DateTime::default();
        unsafe { ui::uiDateTimePickerTime(self.raw, &mut time as *mut DateTime as *mut ui::tm) };
        time
    }

    /// Sets the time in the `DateTimePicker` to `time`.
    /// No time zone manipulations are done.
    pub fn set_value(&mut self, time: &DateTime) {
        unsafe { ui::uiDateTimePickerSetTime(self.raw, time as *const DateTime as *const ui::tm) };
    }

    /// Registers `f` to be run when the user changes the time in the `DateTimePicker`.
    ///
    /// Only one function can be registered at a time.
    pub fn on_changed<F: FnMut(&mut DateTimePicker)>(&mut self, f: F) {
        extern "C" fn c_callback<F: FnMut(&mut DateTimePicker)>(
            buttom: *mut ui::uiDateTimePicker,
            data: *mut c_void,
        ) {
            let closure: &mut F = unsafe { &mut *(data as *mut F) };
            let mut picker = unsafe { DateTimePicker::from_raw(buttom) };
            (*closure)(&mut picker);
        }

        let data = Box::into_raw(Box::new(f)) as *mut c_void;
        unsafe { ui::uiDateTimePickerOnChanged(self.raw, Some(c_callback::<F>), data) }
    }
}

control! {
    /// `MultilineEntry` is a `Control` that represents a space that the user can
    /// type multiple lines of text into.
    MultilineEntry,
    uiMultilineEntry
}

impl MultilineEntry {
    /// Creates a new `MultilineEntry`.
    /// The `MultilineEntry` soft-word-wraps and has no horizontal scrollbar.
    pub fn new(_ui: &UI) -> MultilineEntry {
        unsafe { MultilineEntry::from_raw(ui::uiNewMultilineEntry()) }
    }

    /// Creates a new `MultilineEntry`.
    /// The `MultilineEntry` does not word-wrap and thus has horizontal scrollbar.
    pub fn non_wrapping(_ui: &UI) -> MultilineEntry {
        unsafe { MultilineEntry::from_raw(ui::uiNewNonWrappingMultilineEntry()) }
    }

    /// Returns the `MultilineEntry`'s text.
    pub fn text(&self) -> String {
        ui_string(unsafe { ui::uiMultilineEntryText(self.raw) })
    }

    /// Sets the `MultilineEntry`'s text to `text`.
    pub fn set_text(&mut self, text: &str) {
        let text = ffi::CString::new(text).unwrap();
        unsafe { ui::uiMultilineEntrySetText(self.raw, text.as_c_str().as_ptr()) };
    }

    /// Append adds text to the end of the `MultilineEntry`'s text.
    ///
    /// TODO selection and scroll behavior
    pub fn append(&mut self, text: &str) {
        let text = ffi::CString::new(text).unwrap();
        unsafe { ui::uiMultilineEntryAppend(self.raw, text.as_c_str().as_ptr()) };
    }

    /// Returns whether the `MultilineEntry` can be changed.
    pub fn read_only(&self) -> bool {
        unsafe { ui::uiMultilineEntryReadOnly(self.raw) != 0 }
    }

    /// Sets whether the `MultilineEntry` can be changed.
    pub fn set_read_only(&mut self, val: bool) {
        unsafe { ui::uiMultilineEntrySetReadOnly(self.raw, val as c_int) };
    }

    /// Registers `f` to be run when the user makes a change to the `MultilineEntry`.
    ///
    /// Only one function can be registered at a time.
    pub fn on_changed<F: FnMut(&mut MultilineEntry)>(&mut self, f: F) {
        extern "C" fn c_callback<F: FnMut(&mut MultilineEntry)>(
            control: *mut ui::uiMultilineEntry,
            data: *mut c_void,
        ) {
            let closure: &mut F = unsafe { &mut *(data as *mut F) };
            let mut control = unsafe { MultilineEntry::from_raw(control) };
            (*closure)(&mut control);
        }

        let data = Box::into_raw(Box::new(f)) as *mut c_void;
        unsafe { ui::uiMultilineEntryOnChanged(self.raw, Some(c_callback::<F>), data) }
    }
}
