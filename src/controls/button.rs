use std::ffi;
use std::os::raw::{c_int, c_void};

use super::{Control, ControlHandle, ControlImpl};
use crate::{ui_string, UI};

use libui_sys as ui;

control! {
    /// `Button` is a `Control` that represents a button that the user can click to
    /// perform an action.
    /// A Button has a text label that should describe what the button does.
    Button,
    uiButton
}

impl Button {
    /// Creates a new `Button` with the given `text` as its label.
    pub fn new(_ui: &UI, text: &str) -> Button {
        let text = ffi::CString::new(text).unwrap();
        unsafe { Button::from_raw(ui::uiNewButton(text.as_c_str().as_ptr())) }
    }

    /// Returns the `Button`'s text.
    pub fn text(&self) -> String {
        ui_string(unsafe { ui::uiButtonText(self.raw) })
    }

    /// Sets the Button's `text`.
    pub fn set_text(&mut self, text: &str) {
        let text = ffi::CString::new(text).unwrap();
        unsafe { ui::uiButtonSetText(self.raw, text.as_c_str().as_ptr()) };
    }

    /// Registers `f` to be run when the user clicks the `Button`.
    ///
    /// Only one function can be registered at a time.
    pub fn on_clicked<F: FnMut(&mut Button)>(&mut self, f: F) {
        extern "C" fn c_callback<F: FnMut(&mut Button)>(
            control: *mut ui::uiButton,
            data: *mut c_void,
        ) {
            let closure: &mut F = unsafe { &mut *(data as *mut F) };
            let mut control = unsafe { Button::from_raw(control) };
            (*closure)(&mut control);
        }

        let data = Box::into_raw(Box::new(f)) as *mut c_void;
        unsafe { ui::uiButtonOnClicked(self.raw, Some(c_callback::<F>), data) }
    }
}

control! {
    /// `ColorButton` is a `Control` that represents a button that the user can
    /// click to select a color.
    ColorButton,
    uiColorButton
}

impl ColorButton {
    /// Creates a new `ColorButton`.
    pub fn new(_ui: &UI) -> ColorButton {
        unsafe { ColorButton::from_raw(ui::uiNewColorButton()) }
    }

    /// Returns the color currently selected in the `ColorButton`.
    /// Colors are not alpha-premultiplied.
    pub fn color(&self) -> (f64, f64, f64, f64) {
        let mut color = (0.0, 0.0, 0.0, 0.0);
        unsafe {
            ui::uiColorButtonColor(
                self.raw,
                &mut color.0,
                &mut color.1,
                &mut color.2,
                &mut color.3,
            )
        };
        color
    }

    /// Sets the currently selected color in the `ColorButton`.
    /// Colors are not alpha-premultiplied.
    pub fn set_color(&mut self, r: f64, g: f64, b: f64, a: f64) {
        unsafe { ui::uiColorButtonSetColor(self.raw, r, g, b, a) };
    }

    /// Registers `f` to be run when the user clicks the `ColorButton`.
    ///
    /// Only one function can be registered at a time.
    pub fn on_changed<F: FnMut(&mut ColorButton)>(&mut self, f: F) {
        extern "C" fn c_callback<F: FnMut(&mut ColorButton)>(
            control: *mut ui::uiColorButton,
            data: *mut c_void,
        ) {
            let closure: &mut F = unsafe { &mut *(data as *mut F) };
            let mut control = unsafe { ColorButton::from_raw(control) };
            (*closure)(&mut control);
        }

        let data = Box::into_raw(Box::new(f)) as *mut c_void;
        unsafe { ui::uiColorButtonOnChanged(self.raw, Some(c_callback::<F>), data) }
    }
}

control! {
    /// `Checkbox` is a `Control` that represents a box with a text label at its side.
    /// When the user clicks the checkbox, a check mark will appear in the box;
    /// clicking it again removes the check.
    Checkbox,
    uiCheckbox
}

impl Checkbox {
    /// Creates a new `Checkbox` with the given `text` as its label.
    pub fn new(_ui: &UI, text: &str) -> Checkbox {
        let text = ffi::CString::new(text).unwrap();
        unsafe { Checkbox::from_raw(ui::uiNewCheckbox(text.as_c_str().as_ptr())) }
    }

    /// Returns the `Checkbox`'s text.
    pub fn text(&self) -> String {
        ui_string(unsafe { ui::uiCheckboxText(self.raw) })
    }

    /// Sets the `Checkbox`'s text to `text`.
    pub fn set_text(&mut self, text: &str) {
        let text = ffi::CString::new(text).unwrap();
        unsafe { ui::uiCheckboxSetText(self.raw, text.as_c_str().as_ptr()) };
    }

    /// Returns whether the `Checkbox` is checked.
    pub fn checked(&self) -> bool {
        unsafe { ui::uiCheckboxChecked(self.raw) != 0 }
    }

    /// Sets whether the `Checkbox` is checked.
    pub fn set_checked(&mut self, val: bool) {
        unsafe { ui::uiCheckboxSetChecked(self.raw, val as c_int) };
    }

    /// Registers `f` to be run when the user clicks the `Checkbox`.
    ///
    /// Only one function can be registered at a time.
    pub fn on_toggled<F: FnMut(&mut Checkbox)>(&mut self, f: F) {
        extern "C" fn c_callback<F: FnMut(&mut Checkbox)>(
            control: *mut ui::uiCheckbox,
            data: *mut c_void,
        ) {
            let closure: &mut F = unsafe { &mut *(data as *mut F) };
            let mut control = unsafe { Checkbox::from_raw(control) };
            (*closure)(&mut control);
        }

        let data = Box::into_raw(Box::new(f)) as *mut c_void;
        unsafe { ui::uiCheckboxOnToggled(self.raw, Some(c_callback::<F>), data) }
    }
}

control! {
    /// `RadioButtons` is a `Control` that represents a set of checkable buttons
    /// from which exactly one may be chosen by the user.
    RadioButtons,
    uiRadioButtons
}

impl RadioButtons {
    /// Creates new `RadioButtons`.
    pub fn new(_ui: &UI) -> RadioButtons {
        unsafe { RadioButtons::from_raw(ui::uiNewRadioButtons()) }
    }

    /// Adds the named button to the end of the `RadioButtons`.
    pub fn append(&mut self, name: &str) {
        let name = ffi::CString::new(name).unwrap();
        unsafe { ui::uiRadioButtonsAppend(self.raw, name.as_c_str().as_ptr()) };
    }

    /// Returns the index of the currently selected option in the RadioButtons,
    /// or -1 if no item is selected.
    pub fn selected(&self) -> isize {
        unsafe { ui::uiRadioButtonsSelected(self.raw) as isize }
    }

    /// Sets the currently selected option in the `RadioButtons` to `index`.
    pub fn set_selected(&mut self, index: isize) {
        unsafe { ui::uiRadioButtonsSetSelected(self.raw, index as c_int) };
    }

    /// Registers `f` to be run when the user selects an option in the `RadioButtons`.
    ///
    /// Only one function can be registered at a time.
    pub fn on_changed<F: FnMut(&mut RadioButtons)>(&mut self, f: F) {
        extern "C" fn c_callback<F: FnMut(&mut RadioButtons)>(
            control: *mut ui::uiRadioButtons,
            data: *mut c_void,
        ) {
            let closure: &mut F = unsafe { &mut *(data as *mut F) };
            let mut control = unsafe { RadioButtons::from_raw(control) };
            (*closure)(&mut control);
        }

        let data = Box::into_raw(Box::new(f)) as *mut c_void;
        unsafe { ui::uiRadioButtonsOnSelected(self.raw, Some(c_callback::<F>), data) }
    }
}
