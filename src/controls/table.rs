use std::cell::{RefCell, RefMut};
use std::ffi;
use std::os::raw::{c_int, c_void};
use std::rc::Rc;

use super::{Control, ControlHandle, ControlImpl};
use crate::UI;

use libui_sys as ui;

control! {
    /// `Table` is a `Control` that shows tabular data, allowing users to manipulate rows of such data at a time.
    Table,
    uiTable
}

impl Table {
    /// Creates a new `Table` with the specified parameters.
    pub fn new<'a, H: TableModelHandler>(_ui: &UI, params: TableParams<'a, H>) -> Table {
        let mut params = ui::uiTableParams {
            Model: params.model.raw,
            RowBackgroundColorModelColumn: params.row_background as c_int,
        };
        unsafe { Table::from_raw(ui::uiNewTable(&mut params)) }
    }

    /// Appends a text column to the `Table`.
    ///
    /// `name` is displayed in the table header.
    /// `text_column` is where the text comes from.
    /// If a row is editable according to `editable_column`,
    /// `TableModelHandler::set_cell_value` is called with `text_column` as the column.
    pub fn add_column_text(&mut self, name: &str, text_column: usize) {
        let name = ffi::CString::new(name).unwrap();
        unsafe {
            ui::uiTableAppendTextColumn(
                self.raw,
                name.as_c_str().as_ptr(),
                text_column as c_int,
                ui::uiTableModelColumnNeverEditable,
                std::ptr::null_mut(),
            )
        }
    }

    /// Appends an editable text column to `Table`.
    ///
    /// `name` is displayed in the table header.
    /// `text_column` is where the text comes from.
    /// If a row is editable according to `editable_column`,
    /// `TableModelHandler::set_cell_value` is called with `text_column` as the column.
    pub fn add_column_text_editable(&mut self, name: &str, text_column: usize) {
        let name = ffi::CString::new(name).unwrap();
        unsafe {
            ui::uiTableAppendTextColumn(
                self.raw,
                name.as_c_str().as_ptr(),
                text_column as c_int,
                ui::uiTableModelColumnAlwaysEditable,
                std::ptr::null_mut(),
            )
        }
    }
}

/// `TableParams` defines the parameters passed to `Table::new`.
#[derive(Debug)]
pub struct TableParams<'a, H: TableModelHandler> {
    /// The `TableModel` to use for this uiTable.
    pub model: &'a mut TableModel<H>,

    /// A model column number that defines the background color used for the
    /// entire row in the Table, or -1 to use the default color for all rows.
    ///
    /// If CellValue for this column for any row returns NULL, that
    /// row will also use the default background color.
    pub row_background: isize,
}

impl<'a, H: TableModelHandler> TableParams<'a, H> {
    /// Creates new `TableParams`.
    pub fn new(model: &'a mut TableModel<H>, row_background: isize) -> TableParams<'a, H> {
        TableParams {
            model,
            row_background,
        }
    }
}

/// `TableModel` is an object that provides the data for a `Table`.
/// This data is returned via methods you implement in the
/// `TableModelHandler`.
///
/// A TableModel represents data used by a table, but this table does
/// not map directly to Table itself. Instead, you can have data
/// columns which provide instructions for how to render a given
/// Table's column — for instance, one model column can be used
/// to give certain rows of a Table a different background color.
/// Row numbers DO match with Table row numbers.
///
/// Once created, the number and data types of columns of a
/// TableModel cannot change.
///
/// Row and column numbers start at 0. A `TableModel` can be
/// associated with more than one `Table` at a time.
///
/// A TableModel may be shared between multiple Tables.
#[derive(Debug)]
pub struct TableModel<H: TableModelHandler> {
    raw: *mut ui::uiTableModel,
    handler: Rc<RefCell<H>>,
}

impl<H: TableModelHandler> TableModel<H> {
    /// Creates a new TableModel with the given handler.
    pub fn new(handler: H) -> TableModel<H> {
        let handler = Rc::new(RefCell::new(handler));
        let callbacks = ui::uiTableModelHandler {
            NumColumns: Some(TableModel::<H>::num_columns),
            ColumnType: Some(TableModel::<H>::column_type),
            NumRows: Some(TableModel::<H>::num_rows),
            CellValue: Some(TableModel::<H>::cell_value),
            SetCellValue: Some(TableModel::<H>::set_cell_value),
        };

        TableModel {
            raw: unsafe { ui::uiNewTableModel(callbacks, handler.as_ptr() as *mut _) },
            handler,
        }
    }

    /// Returns a reference to inner table handler, which manages the data.
    pub fn handler(&mut self) -> RefMut<H> {
        self.handler.borrow_mut()
    }

    /// Tells any `Table` associated with m
    /// that a new row has been added to m at index index. You call
    /// this function when the number of rows in your model has
    /// changed;
    ///
    /// After calling it, `TableModelHandler::num_rows` should return the new
    /// row count.
    pub fn row_inserted(&self, new_index: usize) {
        unsafe { ui::uiTableModelRowInserted(self.raw, new_index as c_int) };
    }

    /// Tells any `Table` associated with m
    /// that the data in the row at index has changed.
    ///
    /// You do not need to call this in your `TableModelHandler::set_cell_value`
    /// handlers, but you do need to call this if your data changes at some
    /// other point.
    pub fn row_changed(&self, index: usize) {
        unsafe { ui::uiTableModelRowChanged(self.raw, index as c_int) };
    }

    /// Tells any `Table` associated with m
    /// that the row at index index has been deleted. You call this
    /// function when the number of rows in your model has changed;
    ///
    /// After calling it, `TableModelHandler::num_rows` should return the new
    /// row count.
    pub fn row_deleted(&self, old_index: usize) {
        unsafe { ui::uiTableModelRowDeleted(self.raw, old_index as c_int) };
    }

    /// Raw handler method
    extern "C" fn num_columns(_: *mut ui::uiTableModel, handler: *mut c_void) -> c_int {
        let handler = unsafe { &*(handler as *mut H) };
        handler.num_columns() as c_int
    }

    /// Raw handler method
    extern "C" fn column_type(
        _: *mut ui::uiTableModel,
        handler: *mut c_void,
        column: c_int,
    ) -> ui::uiTableValueType {
        let handler = unsafe { &*(handler as *mut H) };
        handler.column_type(column as usize) as ui::uiTableValueType
    }

    /// Raw handler method
    extern "C" fn num_rows(_: *mut ui::uiTableModel, handler: *mut c_void) -> c_int {
        let handler = unsafe { &*(handler as *mut H) };
        handler.num_rows() as c_int
    }

    /// Raw handler method
    extern "C" fn cell_value(
        _: *mut ui::uiTableModel,
        handler: *mut c_void,
        row: c_int,
        column: c_int,
    ) -> *mut ui::uiTableValue {
        let handler = unsafe { &*(handler as *mut H) };
        handler.cell_value(row as usize, column as usize).into_raw()
    }

    /// Raw handler method
    extern "C" fn set_cell_value(
        _: *mut ui::uiTableModel,
        handler: *mut c_void,
        row: c_int,
        column: c_int,
        val: *const ui::uiTableValue,
    ) {
        let handler = unsafe { &mut *(handler as *mut H) };
        let ty = handler.column_type(column as usize);
        handler.set_cell_value(
            row as usize,
            column as usize,
            TableValue::from_raw(val as *mut ui::uiTableValue, ty),
        );
    }
}

impl<H: TableModelHandler> Drop for TableModel<H> {
    fn drop(&mut self) {
        unsafe { ui::uiFreeTableModel(self.raw) };
    }
}

/// `TableModelHandler` defines the methods that TableModel calls when it needs data.
pub trait TableModelHandler: std::fmt::Debug {
    /// Returns the number of model columns in the `TableModel`.
    /// This value must remain constant through the lifetime of the `TableModel`.
    ///
    /// This method is not guaranteed to be called depending on the system.
    fn num_columns(&self) -> usize;

    /// Returns the value type of the data stored in
    /// the given model column of the `TableModel`. The returned
    /// values must remain constant through the lifetime of the
    /// `TableModel`.
    ///
    /// This method is not guaranteed to be called depending on the system.
    fn column_type(&self, column: usize) -> TableValueType;

    /// Returns the number or rows in the `TableModel`.
    fn num_rows(&self) -> usize;

    /// Returns a `TableValue` corresponding to the model
    /// cell at (row, column). The type of the returned `TableValue`
    /// must match column's value type. Under some circumstances,
    /// `TableValue::Null` may be returned;
    /// refer to the various methods that add
    /// columns to `Table` for details.
    ///
    /// Once returned, the `Table` that calls `cell_value` will free the
    /// `TableValue` returned.
    fn cell_value(&self, row: usize, column: usize) -> TableValue;

    /// Changes the model cell value at (row, column)
    /// in the `TableModel`. Within this function, either do nothing
    /// to keep the current cell value or save the new cell value as
    /// appropriate. After `set_cell_value` is called, the `Table` will
    /// itself reload the table cell. Under certain conditions, the
    /// `TableValue` passed in can be TableValue::Null`;
    /// refer to the various methods that add columns to `Table` for details.
    ///
    /// Once returned, the `Table` that called `set_cell_value` will free the
    /// `TableValue` passed in.
    fn set_cell_value(&mut self, row: usize, column: usize, value: TableValue);
}

#[derive(Debug, Clone, Copy)]
/// `TableValueType` represents the type of data that can come out of a `TableModel`.
pub enum TableValueType {
    Str = 0,
    /// Image = 1, // **Not implemented yet**
    Int = 2,
    Color = 3,
}

/// `TableValue` is a type that represents a piece of data that can come out of a `TableModel`.
#[derive(Debug, Clone)]
pub enum TableValue {
    Str(String),
    Int(i32),
    Color(f64, f64, f64, f64),
    /// Used for some column types like the button column
    Null,
}

impl TableValue {
    fn from_raw(raw: *mut ui::uiTableValue, ty: TableValueType) -> TableValue {
        if raw.is_null() {
            TableValue::Null
        } else {
            match ty {
                TableValueType::Str => TableValue::Str(unsafe {
                    ffi::CStr::from_ptr(ui::uiTableValueString(raw))
                        .to_string_lossy()
                        .into_owned()
                }),
                TableValueType::Int => TableValue::Int(unsafe { ui::uiTableValueInt(raw) }),
                TableValueType::Color => {
                    let (mut r, mut g, mut b, mut a) = (0.0, 0.0, 0.0, 0.0);
                    unsafe { ui::uiTableValueColor(raw, &mut r, &mut g, &mut b, &mut a) };
                    TableValue::Color(r, g, b, a)
                }
            }
        }
    }

    fn into_raw(self) -> *mut ui::uiTableValue {
        match self {
            TableValue::Str(val) => unsafe {
                let val = ffi::CString::new(val).unwrap();
                ui::uiNewTableValueString(val.as_c_str().as_ptr())
            },
            TableValue::Int(val) => unsafe { ui::uiNewTableValueInt(val) },
            TableValue::Color(r, g, b, a) => unsafe { ui::uiNewTableValueColor(r, g, b, a) },
            TableValue::Null => std::ptr::null_mut(),
        }
    }
}

pub trait TableValueImpl: Sized + Clone + std::fmt::Debug {
    fn into_value(self) -> TableValue;
    fn from_value(value: TableValue) -> Option<Self>;
    fn value_type() -> TableValueType;
}

impl TableValueImpl for () {
    fn into_value(self) -> TableValue {
        TableValue::Null
    }

    fn from_value(_: TableValue) -> Option<()> {
        None
    }

    fn value_type() -> TableValueType {
        TableValueType::Str // TODO: Null type...
    }
}

impl TableValueImpl for String {
    fn into_value(self) -> TableValue {
        TableValue::Str(self)
    }

    fn from_value(value: TableValue) -> Option<String> {
        match value {
            TableValue::Str(s) => Some(s),
            _ => None,
        }
    }

    fn value_type() -> TableValueType {
        TableValueType::Str
    }
}

impl TableValueImpl for i32 {
    fn into_value(self) -> TableValue {
        TableValue::Int(self)
    }

    fn from_value(value: TableValue) -> Option<i32> {
        match value {
            TableValue::Int(s) => Some(s),
            _ => None,
        }
    }

    fn value_type() -> TableValueType {
        TableValueType::Int
    }
}

impl TableValueImpl for (f64, f64, f64, f64) {
    fn into_value(self) -> TableValue {
        TableValue::Color(self.0, self.1, self.2, self.3)
    }

    fn from_value(value: TableValue) -> Option<(f64, f64, f64, f64)> {
        match value {
            TableValue::Color(r, g, b, a) => Some((r, g, b, a)),
            _ => None,
        }
    }

    fn value_type() -> TableValueType {
        TableValueType::Color
    }
}

// Table handler implementations:

/// Simple table store with a single column that can be used for a TableModel.
#[derive(Debug)]
pub struct TableModelStore<T>(Vec<T>);

impl<T: TableValueImpl> TableModelHandler for TableModelStore<T> {
    fn num_columns(&self) -> usize {
        1
    }
    fn column_type(&self, _column: usize) -> TableValueType {
        T::value_type()
    }
    fn num_rows(&self) -> usize {
        self.0.len()
    }
    fn cell_value(&self, row: usize, _column: usize) -> TableValue {
        self.0[row].clone().into_value()
    }
    fn set_cell_value(&mut self, row: usize, _column: usize, value: TableValue) {
        TableValueImpl::from_value(value).map_or((), |v| self.0[row] = v);
    }
}

impl<T> From<Vec<T>> for TableModelStore<T> {
    fn from(val: Vec<T>) -> Self {
        TableModelStore(val)
    }
}

/// Simple table store with multiple columns of the same type that can be used for a TableModel.
/// # Warning
/// The number of columns have to remain static during the lifetime of the table model!
#[derive(Debug)]
pub struct TableModelStoreVec<T> {
    pub data: Vec<Vec<T>>,
    pub columns: usize,
}

impl<T: TableValueImpl> TableModelHandler for TableModelStoreVec<T> {
    fn num_columns(&self) -> usize {
        self.columns
    }
    fn column_type(&self, _column: usize) -> TableValueType {
        T::value_type()
    }
    fn num_rows(&self) -> usize {
        self.data.len()
    }
    fn cell_value(&self, row: usize, column: usize) -> TableValue {
        self.data[row][column].clone().into_value()
    }
    fn set_cell_value(&mut self, row: usize, column: usize, value: TableValue) {
        TableValueImpl::from_value(value).map_or((), |v| self.data[row][column] = v);
    }
}

macro_rules! table_store_tuple {
    ($cols:literal; $( $types:ident ),+; $( $indices:tt ),+; ) => {
        impl<$( $types: TableValueImpl, )+> TableModelHandler for Vec<($( $types ),+)>
        {
            fn num_columns(&self) -> usize {
                $cols
            }
            fn column_type(&self, column: usize) -> TableValueType {
                match column {
                    $( $indices => $types::value_type(), )+
                    _ => TableValueType::Str, // TODO: Null type...
                }
            }
            fn num_rows(&self) -> usize {
                self.len()
            }
            fn cell_value(&self, row: usize, column: usize) -> TableValue {
                match column {
                    $( $indices => self[row].$indices.clone().into_value(), )+
                    _ => TableValue::Null,
                }
            }
            fn set_cell_value(&mut self, row: usize, column: usize, value: TableValue) {
                match column {
                    $(
                        $indices => TableValueImpl::from_value(value).map_or(
                            (), |v| self[row].$indices = v),
                    )+
                    _ => (),
                }
            }
        }
    };
}

table_store_tuple! {
    2;
    A, B;
    0, 1;
}

table_store_tuple! {
    3;
    A, B, C;
    0, 1, 2;
}

table_store_tuple! {
    4;
    A, B, C, D;
    0, 1, 2, 3;
}

table_store_tuple! {
    5;
    A, B, C, D, E;
    0, 1, 2, 3, 4;
}

table_store_tuple! {
    6;
    A, B, C, D, E, F;
    0, 1, 2, 3, 4, 5;
}
table_store_tuple! {
    7;
    A, B, C, D, E, F, G;
    0, 1, 2, 3, 4, 5, 6;
}
table_store_tuple! {
    8;
    A, B, C, D, E, F, G, H;
    0, 1, 2, 3, 4, 5, 6, 7;
}
table_store_tuple! {
    9;
    A, B, C, D, E, F, G, H, I;
    0, 1, 2, 3, 4, 5, 6, 7, 8;
}
table_store_tuple! {
    10;
    A, B, C, D, E, F, G, H, I, J;
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9;
}
table_store_tuple! {
    11;
    A, B, C, D, E, F, G, H, I, J, K;
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10;
}
table_store_tuple! {
    12;
    A, B, C, D, E, F, G, H, I, J, K, L;
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11;
}
