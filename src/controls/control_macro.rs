// Macro for defining the necessary control traits
macro_rules! control {
    ($(#[$attr:meta])* $name:ident, $ptr:ident) => {
        $(#[$attr])*
        #[derive(Debug, Clone)]
        pub struct $name {
            raw: *mut ui::$ptr,
        }

        impl ControlImpl for $name {
            fn ptr(&self) -> *mut ui::uiControl {
                self.raw as *mut ui::uiControl
            }
        }

        impl $name {
            /// Creates a handle that can be shared between threads
            pub fn thread_handle(&mut self) -> ControlHandle<$name> {
                ControlHandle {
                    control: self as *mut _ as *mut $name,
                }
            }

            /// Creates a new Control from the corresponding libui pointer.
            ///
            /// # Safety
            /// The pointer has to be a valid pointer for this type of Control.
            pub unsafe fn from_raw(raw: *mut ui::$ptr) -> $name {
                $name { raw }
            }
        }

        impl From<Control> for $name {
            fn from(control: Control) -> Self {
                unsafe { Self::from_raw(control.ptr() as *mut _) }
            }
        }

        impl Into<Control> for $name {
            fn into(self) -> Control {
                unsafe { Control::from_raw(self.ptr()) }
            }
        }
    };
}
