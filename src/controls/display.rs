use std::ffi;

use super::{Control, ControlHandle, ControlImpl};
use crate::{ui_string, UI};

use libui_sys as ui;

control! {
    /// `Label` is a `Control` that represents a line of text that cannot be interacted with.
    Label,
    uiLabel
}

impl Label {
    /// Creates a new `Label` with the given `text`.
    pub fn new(_ui: &UI, text: &str) -> Label {
        let text = ffi::CString::new(text).unwrap();
        unsafe { Label::from_raw(ui::uiNewLabel(text.as_c_str().as_ptr())) }
    }

    /// Returns the `Label`'s text.
    pub fn text(&self) -> String {
        ui_string(unsafe { ui::uiLabelText(self.raw) })
    }

    /// Sets the `Label`'s text to `text`.
    pub fn set_text(&mut self, text: &str) {
        let text = ffi::CString::new(text).unwrap();
        unsafe { ui::uiLabelSetText(self.raw, text.as_c_str().as_ptr()) };
    }
}

control! {
    /// `ProgressBar` is a `Control` that represents a horizontal bar that is
    /// filled in progressively over time as a process completes.
    ProgressBar,
    uiProgressBar
}

impl ProgressBar {
    /// Creates a new `ProgressBar`.
    pub fn new(_ui: &UI) -> ProgressBar {
        unsafe { ProgressBar::from_raw(ui::uiNewProgressBar()) }
    }

    /// Returns the value currently shown in the `ProgressBar`.
    pub fn value(&self) -> i32 {
        unsafe { ui::uiProgressBarValue(self.raw) }
    }

    /// Sets the `ProgressBar`'s currently displayed percentage to `value`.
    /// `value` must be between 0 and 100 inclusive, or -1 for an indeterminate progressbar.
    pub fn set_value(&mut self, value: i32) {
        unsafe { ui::uiProgressBarSetValue(self.raw, value) };
    }
}

control! {
    /// `Separator` is a `Control` that represents a horizontal line that
    /// visually separates controls.
    Separator,
    uiSeparator
}

impl Separator {
    /// Creates a new horizontal `Separator`.
    pub fn horizontal(_ui: &UI) -> Separator {
        unsafe { Separator::from_raw(ui::uiNewHorizontalSeparator()) }
    }

    /// Creates a new vertical `Separator`.
    pub fn vertical(_ui: &UI) -> Separator {
        unsafe { Separator::from_raw(ui::uiNewVerticalSeparator()) }
    }
}
