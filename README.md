# libui-rs

Bindings for the minimalist, cross-platform, widget set `libui`.

The `libui` library is currently mid-alpha, see its [repository](https://github.com/andlabs/libui) for more information.

## Documentation

Find it on [Docs.rs](https://docs.rs/libui-rs).

## Example

Add `libui-rs` to your dependencies of your `Cargo.toml`:

```toml
[dependencies]
libui-rs = "0.1.0"
```

And than in your `main.rs`:

```rust
fn main() {
    let ui = UI::init().unwrap();

    let mut window = Window::new(&ui, "Hello", 320, 240, false);

    let mut label = Label::new(&ui, "Hello, libui!");

    let mut label_btn = Button::new(&ui, "Change Label");
    let mut counter = 0;
    label_btn.on_clicked(|_| {
        counter += 1;
        label.set_text(&format!("Button clicked {} times", counter));
    });
    let mut file_btn = Button::new(&ui, "Open File");
    file_btn.on_clicked(|_| {
        window.open_file();
    });

    let mut hbox = BoxLayout::vertical(&ui);
    hbox.append(&mut label, true);
    hbox.append(&mut label_btn, false);
    hbox.append(&mut file_btn, false);

    window.set_margined(true);
    window.set_child(&mut hbox);
    window.show();

    // Start main loop
    ui.set_visible(true);
}
```

# Runtime Requirements

* Windows: Windows 10
* Unix: GTK+ 3.18 or newer
* Mac OS X: OS X 10.13 or newer

> Only modern currently maintained os versions are guaranted to work.
>
> This decision was made to reduce the amount of legecy and compatability code
> necessary to support older os versions and speed up the development of new
> features.

# Build Requirements

On all platforms `cmake` 3.1.0 or newer is needed for building the `libui` library.
Also `libclang` is needed for `bindgen` to generate the C bindings.

On Windows:
* Microsoft Visual Studio 2013 or newer
* MinGW-w64 (other flavors of MinGW may not work)

On Linux the gtk3 development libraries have to be installed:
* Debian, Ubuntu, etc.: sudo apt-get install libgtk-3-dev
* Red Hat/Fedora, etc.: sudo dnf install gtk3-devel

Mac OS X: nothing else specific, so long as you can build Cocoa programs

### Cross-Compilation: Linux to Windows

For cross-compilation the [cross](https://github.com/rust-embedded/cross) tool is used, which is a fancy wrapper around docker.
Because this project uses `bindgen`, which depends on `libclang` there is a special docker image ([Dockerfile](docker/cross-win/Dockerfile)) that specifies those dependencies.
This docker image has to be build only on the first time.
```bash
docker build -t custom/cross-win-libui docker/cross-win
```

Then the build process can be started by executing:
```bash
cross build --target=x86_64-pc-windows-gnu
```
