use libui::*;

fn main() {
    let ui = UI::init().unwrap();
    let mut window = Window::new(&ui, "UI Table Test", 320, 240, false);

    let mut model1: TableModel<TableModelStore<i32>> = TableModel::new(vec![1, 2, 3, 4, 5].into());
    println!("model1 type: {:?}", model1.handler().column_type(0));

    let mut model = TableModel::new(vec![
        (String::from("bla"), String::from("bla")),
        (String::from("blub"), String::from("jo")),
    ]);
    let params = TableParams::new(&mut model, -1);
    let mut table = Table::new(&ui, params);
    table.add_column_text_editable("first", 0);
    table.add_column_text("second", 1);

    let mut add_row_btn = Button::new(&ui, "Add Row");
    add_row_btn.on_clicked(|_| {
        let new_index = model.handler().len();
        model.handler().push(("new".into(), "row".into()));
        model.row_inserted(new_index);
        println!("Row added {}", new_index);
    });
    let mut add_row_first_btn = Button::new(&ui, "Add Row First");
    add_row_first_btn.on_clicked(|_| {
        model.handler().insert(0, ("new".into(), "row".into()));
        model.row_inserted(0);
        println!("Row added 0");
    });
    let mut rm_row_btn = Button::new(&ui, "Remove Row");
    rm_row_btn.on_clicked(|_| {
        let old_len = model.handler().len();
        if old_len > 0 {
            model.handler().pop();
            model.row_deleted(old_len - 1);
            println!("Row removed {}", old_len - 1);
        }
    });

    let mut vbox = BoxLayout::vertical(&ui);
    vbox.append(&mut table, true);
    vbox.append(&mut add_row_btn, false);
    vbox.append(&mut add_row_first_btn, false);
    vbox.append(&mut rm_row_btn, false);

    window.set_margined(true);
    window.set_child(&mut vbox);
    window.set_visible(true);

    println!("ui main...");
    ui.main();
}
