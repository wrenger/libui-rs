use libui::*;

fn main() {
    let ui = UI::init().unwrap();

    let mut window = Window::new(&ui, "Hello", 320, 240, false);

    let mut label = Label::new(&ui, "Hello, libui!");

    let mut label_btn = Button::new(&ui, "Change Label");
    let mut counter = 0;
    label_btn.on_clicked(|_| {
        counter += 1;
        label.set_text(&format!("Button clicked {} times", counter));
    });
    let mut file_btn = Button::new(&ui, "Open File");
    file_btn.on_clicked(|_| {
        println!("Open {:?}", window.open_file());
    });

    let mut hbox = BoxLayout::vertical(&ui);
    hbox.append(&mut label, true);
    hbox.append(&mut label_btn, false);
    hbox.append(&mut file_btn, false);

    window.set_margined(true);
    window.set_child(&mut hbox);
    window.set_visible(true);

    // Start main loop
    ui.main();
}
