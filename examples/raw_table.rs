use libui_sys as ui;
use std::ffi::{CStr, CString};
use std::os::raw::{c_char, c_int, c_void};

fn main() {
    unsafe {
        println!("Starting raw table example...");
        let mut o = ui::uiInitOptions { Size: 0 };
        if !ui::uiInit(&mut o as *mut ui::uiInitOptions).is_null() {
            panic!("Error initializing ui");
        }

        let window = ui::uiNewWindow(
            b"UI Table Test\0" as *const u8 as *const c_char,
            320,
            240,
            0,
        );
        ui::uiWindowSetMargined(window, 1);

        extern "C" fn num_columns(_model: *mut ui::uiTableModel, _handler: *mut c_void) -> c_int {
            2
        }

        extern "C" fn column_type(
            _model: *mut ui::uiTableModel,
            _handler: *mut c_void,
            _column: c_int,
        ) -> ui::uiTableValueType {
            ui::uiTableValueTypeString as _
        }

        extern "C" fn num_rows(_model: *mut ui::uiTableModel, handler: *mut c_void) -> c_int {
            let data = unsafe { &mut *(handler as *mut Vec<[CString; 2]>) };
            data.len() as c_int
        }

        extern "C" fn cell_value(
            _model: *mut ui::uiTableModel,
            handler: *mut c_void,
            row: c_int,
            column: c_int,
        ) -> *mut ui::uiTableValue {
            let data = unsafe { &mut *(handler as *mut Vec<[CString; 2]>) };
            unsafe {
                ui::uiNewTableValueString(
                    data[row as usize][column as usize].as_c_str().as_ptr(),
                )
            }
        }

        extern "C" fn set_cell_value(
            _model: *mut ui::uiTableModel,
            handler: *mut c_void,
            row: c_int,
            column: c_int,
            val: *const ui::uiTableValue,
        ) {
            let data = unsafe { &mut *(handler as *mut Vec<[CString; 2]>) };
            let value = unsafe { CStr::from_ptr(ui::uiTableValueString(val)).to_owned() };
            data[row as usize][column as usize] = value;
        }

        let mut data = vec![
            [CString::new("foo").unwrap(), CString::new("bar").unwrap()],
            [CString::new("blub").unwrap(), CString::new("jojo").unwrap()],
        ];

        let table_model = ui::uiNewTableModel(
            ui::uiTableModelHandler {
                NumColumns: Some(num_columns),
                ColumnType: Some(column_type),
                NumRows: Some(num_rows),
                CellValue: Some(cell_value),
                SetCellValue: Some(set_cell_value),
            },
            &mut data as *mut _ as *mut _,
        );

        let mut table_params = ui::uiTableParams {
            Model: table_model,
            RowBackgroundColorModelColumn: -1,
        };
        let table = ui::uiNewTable(&mut table_params);

        ui::uiTableAppendTextColumn(
            table,
            b"first\0" as *const _ as *const c_char,
            0,
            ui::uiTableModelColumnAlwaysEditable,
            std::ptr::null_mut(),
        );
        ui::uiTableAppendTextColumn(
            table,
            b"second\0" as *const _ as *const c_char,
            1,
            ui::uiTableModelColumnNeverEditable,
            std::ptr::null_mut(),
        );

        let vbox = ui::uiNewVerticalBox();
        ui::uiBoxAppend(vbox, table as *mut ui::uiControl, 1);

        ui::uiWindowSetChild(window, vbox as *mut ui::uiControl);
        ui::uiControlShow(window as *mut ui::uiControl);

        unsafe extern "C" fn on_closing(
            _window: *mut ui::uiWindow,
            _data: *mut std::ffi::c_void,
        ) -> std::os::raw::c_int {
            ui::uiQuit();
            1
        }
        ui::uiWindowOnClosing(window, Some(on_closing), std::ptr::null_mut());

        ui::uiMain();
    }
}
