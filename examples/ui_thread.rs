use std::thread;
use std::time::Duration;

use libui::*;

fn main() {
    let ui = UI::init().unwrap();

    let mut window = Window::new(&ui, "UI Test", 320, 240, false);
    window.set_margined(true);
    let mut label = Label::new(&ui, "This message is destroying itself in 3 seconds!");
    window.set_child(&mut label);

    let label_handle = label.thread_handle();
    thread::spawn(move || {
        thread::sleep(Duration::from_secs(3));
        label_handle.queue_main(|_, label| label.set_text("---"));
    });

    window.set_visible(true);
    ui.main();
}
