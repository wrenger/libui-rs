use libui::*;

fn main() {
    let ui = UI::init().unwrap();
    let mut menu = Menu::new(&ui, "Some Menu");
    let mut open_item = menu.append("Open File");
    open_item.on_clicked(|_item, window| {
        window.open_file();
    });
    menu.append_check("Toggle");
    menu.append_separator();
    let mut pref_item = menu.append_preferences();
    pref_item.on_clicked(|_item, window| {
        window.alert("Preferences", "None");
    });
    menu.append_about();
    menu.append_quit();

    let mut window = Window::new(&ui, "UI Test", 320, 240, true);
    window.set_margined(true);
    let mut label = Label::new(&ui, "Some window content");
    window.set_child(&mut label);
    window.set_visible(true);

    // Necessary for the quit menu item.
    ui.on_should_quit(|| {
        window.destroy();
        true
    });

    ui.main();
}
