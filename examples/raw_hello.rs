use libui_sys as ui;
use std::os::raw::{c_char, c_int};

fn main() {
    unsafe {
        let mut o = ui::uiInitOptions { Size: 0 };
        if !ui::uiInit(&mut o as *mut ui::uiInitOptions).is_null() {
            panic!("Error initializing ui");
        }

        let window = ui::uiNewWindow(b"Hello\0" as *const u8 as *const c_char, 320, 240, 0);
        ui::uiWindowSetMargined(window, 1);

        let label = ui::uiNewLabel(b"Hello, World!\0" as *const u8 as *const c_char);

        ui::uiWindowSetChild(window, label as *mut ui::uiControl);
        ui::uiControlShow(window as *mut ui::uiControl);

        unsafe extern "C" fn on_closing(
            _window: *mut ui::uiWindow,
            _data: *mut std::ffi::c_void,
        ) -> c_int {
            ui::uiQuit();
            1
        }
        ui::uiWindowOnClosing(window, Some(on_closing), std::ptr::null_mut());

        ui::uiMain();
    }
}
