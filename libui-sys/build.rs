use std::env;
use std::path::PathBuf;

const BINDINGS_DIR: &str = "./libui";
const BINDINGS_HEADER: &str = "./libui/ui.h";

const BINDINGS_OUT: &str = "bindings.rs";

fn main() {
    let builder = bindgen::builder()
        .whitelist_function("ui.*")
        .whitelist_type("ui.*")
        .whitelist_var("ui.*")
        .header(BINDINGS_HEADER);

    let bindings = builder.generate().expect("Bindings generation failed");

    let out_dir = PathBuf::from(env::var("OUT_DIR").unwrap());

    bindings
        .write_to_file(out_dir.join(BINDINGS_OUT))
        .expect("Bindings saving failed");

    // build
    let (libdir, lib) = {
        let target = env::var("TARGET").unwrap();
        let msvc = target.contains("msvc");
        let apple = target.contains("apple");

        let mut cfg = cmake::Config::new(BINDINGS_DIR);
        cfg.build_target("").profile("debug");
        if apple {
            cfg.cxxflag("-stdlib=libc++");
        }

        let mut libdir = cfg.build().join("build/out");
        if msvc {
            libdir.push("Debug");
        }
        let lib = if msvc { "libui" } else { "ui" };

        (libdir, lib)
    };

    println!("cargo:rustc-link-search=native={}", libdir.display());
    println!("cargo:rustc-link-lib={}", lib);
}
